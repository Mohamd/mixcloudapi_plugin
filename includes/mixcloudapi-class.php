<?php
/**
 * Adds Mixcloudapi  widget.
 */

class Mixcloud_Api_Widget extends WP_Widget {

	protected $memcache;
	/**
	 * Register widget with WordPress.
	 * @return void
	 */
	function __construct() {
		parent::__construct(
			'mixcloudapi_widget', // Base ID
			esc_html__( 'Mixcloud api', 'mixcloudapi_domain' ), // Name
			array( 'description' => esc_html__( 'Widget to display Mixcloud user info', 'mixcloudapi_domain' ), ) // Args
		);

		$this->memcache = new Memcache;
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']; // Whatever you want to display before widget (<div>, etc)


		// Widget Content Output
		if ( !empty( $instance['user_name'] ) ) {
			$user_data = $this->getMixcloudUserInfo($instance['user_name'],$instance['memcache_host'],$instance['memcache_port']);
			if (!is_null($user_data)) {
				$user_data = json_decode($user_data);
				?>
				<div class="mixcloud-widget" >
					<img src="<?php echo $user_data->cover_pictures->{"1670wx240h"}?>"  alt="<?php echo $user_data->name . 'cover picture' ?>"/>
					<div class="overlay">
						<p><?php echo $user_data->name ?> | <?php echo $user_data->city ?></p>
						<img src="<?php echo $user_data->pictures->thumbnail ?>" alt="<?php echo $user_data->name . 'profile image' ?>"/>
						<p><?php echo $user_data->follower_count ?> Followers</p>
						<p><?php
							if (strlen($user_data->biog) > 100) {
								// truncate string
								$stringCut = substr($user_data->biog, 0, 100);
								$endPoint = strrpos($stringCut, ' ');

								//if the string doesn't contain any space then it will cut without word basis.
								$string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
								$string .= '... <br><a class="read_more" href="'.$user_data->url.'">Read More</a>';

								echo $string;
							} else {
								echo $user_data->biog;
							}


							?></p>

						<p>
							<button data-url="<?php echo $user_data->url ?>" class="mixcloud-button-profile">Show Profile</button>
						</p>
					</div>
				</div>
				<?php
			}

		}
		echo $args['after_widget']; // Whatever you want to display after widget (</div>, etc)
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$user_name = ! empty( $instance['user_name'] ) ? $instance['user_name'] : esc_html__( '', 'mixcloudapi_domain' );
		$memcache_host = ! empty( $instance['memcache_host'] ) ? $instance['memcache_host'] : esc_html__( 'localhost', 'mixcloudapi_domain' );
		$memcache_port = ! empty( $instance['memcache_port'] ) ? $instance['memcache_port'] : esc_html__( 11211, 'mixcloudapi_domain' );
		?>
		<!-- User Name -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'user_name' ) ); ?>">
				<?php esc_attr_e( 'Mixcloud User Name:', 'mixcloudapi_domain' ); ?>
			</label>

			<input
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'user_name' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'user_name' ) ); ?>"
				type="text"
				value="<?php echo esc_attr( $user_name ); ?>">
		</p>

		<!-- Memcache Host -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'memcache_host' ) ); ?>">
				<?php esc_attr_e( 'Memcache Host Name or IP :', 'mixcloudapi_domain' ); ?>
			</label>

			<input
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'memcache_host' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'memcache_host' ) ); ?>"
				type="text"
				value="<?php echo esc_attr( $memcache_host ); ?>">
		</p>

		<!-- Memcache port -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'memcache_port' ) ); ?>">
				<?php esc_attr_e( 'Memcache Host Name or IP :', 'mixcloudapi_domain' ); ?>
			</label>

			<input
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_id( 'memcache_port' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'memcache_port' ) ); ?>"
				type="text"
				value="<?php echo esc_attr( $memcache_port ); ?>">
		</p>

		<?php
		// connect to memcache with user information
		$this->memcache->connect($memcache_host,$memcache_port);
		if (!class_exists("Memcache") || $this->memcache->getStats() == false) {
			?>
			<p style="color:red">
				memcached is disabled or not installed please follow this
				<a href="https://www.php.net/manual/en/book.memcached.php">link</a> to know how to install or enable it
				or contact with your host provider
				the plugin will work correctly but it will not cache your user data
			</p>
			<?php
		}

		?>

		<?php
	}


	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['user_name'] = ( ! empty( $new_instance['user_name'] ) ) ? strip_tags( $new_instance['user_name'] ) : '';
		$instance['memcache_host'] = ( ! empty( $new_instance['memcache_host'] ) ) ? strip_tags( $new_instance['memcache_host'] ) : '';
		$instance['memcache_port'] = ( ! empty( $new_instance['memcache_port'] ) ) ? strip_tags( $new_instance['memcache_port'] ) : '';

		return $instance;
	}


	protected function getMixcloudUserInfo($user_name,$memcache_host,$memcache_port) {
		//connect to memcache
		try {
			$this->memcache->connect($memcache_host,$memcache_port);
		} catch (\Exception $e) {
			error_log('Mixcloud api - memcached get item error : ');
			error_log(print_r($e->getMessage()));
		}

		$user_data = false;
		//check if memcached has this user
		try {
			$user_data = $this->memcache->get(strtolower($user_name));
		} catch (\Exception $e) {
			error_log('Mixcloud api - memcached get item error : ');
			error_log(print_r($e->getMessage()));
		}

		if (!$user_data) {
			//make request to get user info
			$response = wp_remote_get('https://api.mixcloud.com/'.$user_name);

			//check if user exist
			if ($response['response']['code'] == 200) {
				// return user data
				$user_data = $response['body'];

				// cache user data for 12 hours
				try {
					$this->memcache->set(strtolower($user_name),$response['body'],false,43200);
				} catch (\Exception $e) {
					error_log('Mixcloud api - memcached set item error : ');
					error_log(print_r($e->getMessage()));
				}

			} else {
				$user_data = null;
			}
		}
		return $user_data;
	}
} // class Mixcloudapi