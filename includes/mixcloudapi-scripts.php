<?php


function mixcloudapi_add_scripts() {
	// Load css files
	wp_enqueue_style('mixcloudapi-main-style',plugins_url().'/mixcloudapi/css/style.css');

	// Load javascript files
	wp_enqueue_script('mixcloudapi-main-scripts',plugins_url().'/mixcloudapi/scripts/main.js');

}


// Register action
add_action('wp_enqueue_scripts', 'mixcloudapi_add_scripts');