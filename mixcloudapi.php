<?php

/*
Plugin Name: Mixcloud Api
Plugin URI: Mixcloud.com
Description: Display Mixcloud User Info
Version: 1.0.0
Author: TieLaps
Author URI: http://tielaps.com/
*/

// Exit if accessed directly for security reasons
if(!defined('ABSPATH')){
    exit;
}

// Load Scripts
require_once(plugin_dir_path(__FILE__).'includes/mixcloudapi-scripts.php');


//Load Mixcloud Api Class
require_once(plugin_dir_path(__FILE__).'includes/mixcloudapi-class.php');

// Register Widget
function register_mixcloudapi(){
	register_widget('Mixcloud_Api_Widget');
}

// Hook in function
add_action('widgets_init', 'register_mixcloudapi');