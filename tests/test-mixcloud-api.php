<?php
/**
 * Class SampleTest
 *
 * @package Mixcloudapi
 */

/**
 * Sample test case.
 */
class MixcloudApiTest extends WP_UnitTestCase {

	/**
	 * A single example test.
	 */
	public function test_widget_return_user_data() {
		ob_start();
		the_widget('Mixcloud_Api_Widget',['user_name'=>'lowlight','memcache_host'=>'localhost','memcache_port'=>11211]);
		$widget_html = ob_get_contents();
		ob_end_clean();

		//get data of user from api
		$memcache = new Memcache();
		$memcache->connect('localhost',11211);
		$body = $memcache->get('lowlight') ?? wp_remote_get('https://api.mixcloud.com/lowlight')['body'];
		$body = json_decode($body);


		//the widget have the user name
		$this->assertContains($body->name,$widget_html);

		//the widget display the cover
		$this->assertContains($body->cover_pictures->{"1670wx240h"},$widget_html);

		//the widget display the thumbnail
		$this->assertContains($body->pictures->thumbnail,$widget_html);

		//the widget display the followers
		$this->assertContains((string)$body->follower_count . ' Followers',$widget_html);

	}
}
